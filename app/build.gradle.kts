import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

group = "me.andrew"
version = "1.0"

dependencies {
    implementation(project(":tea"))
    implementation(project(":atlas"))
    implementation(compose.desktop.currentOs)
    implementation("io.arrow-kt:arrow-core:1.1.2")
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.orbit-mvi:orbit-core:4.3.2")
    implementation("io.insert-koin:koin-core:3.2.0")
    implementation("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.3.5")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "AtlasAnimator"
            packageVersion = "1.0.0"
        }
    }
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "11"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "11"
}
tasks.getByName<Test>("test") {
    useJUnitPlatform()
}
