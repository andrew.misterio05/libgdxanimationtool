package me.andrew.design

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun AAButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    content: @Composable RowScope.() -> Unit,
) = Button(
    modifier = modifier,
    onClick = onClick,
    content = content,
)

@Composable
fun AATextButton(modifier: Modifier = Modifier, onClick: () -> Unit, text: String) = AAButton(
    modifier = modifier,
    onClick = onClick,
    content = { AAText(text = text) },
)
