package me.andrew.design

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.awt.ComposeWindow
import javax.swing.JFileChooser
import javax.swing.filechooser.FileNameExtensionFilter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


@Composable
fun AALaunchFileDialog(
    title: String,
    initPath: String,
    type: String,
): State<String?> {
    val coroutine = rememberCoroutineScope()
    val result = remember { mutableStateOf<String?>(null) }

    LaunchedEffect(Unit) {
        coroutine.launch {
            val pathAsync = coroutine.launchFileDialog(title, initPath, type, ComposeWindow())
            result.value = pathAsync.await()
        }
    }
    return result
}

fun CoroutineScope.launchFileDialog(
    title: String,
    initPath: String,
    type: String,
    window: ComposeWindow = ComposeWindow(),
): Deferred<String?> = async(SupervisorJob()) {
    val dialog = JFileChooser(initPath).apply {
        dialogTitle = title
        isAcceptAllFileFilterUsed = false
        addChoosableFileFilter(FileNameExtensionFilter(type, type))
    }
    val result = dialog.showOpenDialog(window)
    kotlin.runCatching { dialog.selectedFile.path.takeIf { result == JFileChooser.APPROVE_OPTION } }.getOrNull()
}
