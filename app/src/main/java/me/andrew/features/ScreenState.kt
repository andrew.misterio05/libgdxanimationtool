package me.andrew.features

interface ScreenState {
    val id: String

    companion object: ScreenState {
        override val id: String = "ScreenState"
    }
}
