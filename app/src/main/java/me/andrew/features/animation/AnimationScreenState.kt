package me.andrew.features.animation

import java.util.UUID
import me.andrew.AtlasData
import me.andrew.Cmd
import me.andrew.features.ScreenState

sealed interface AnimationTimelineCmd: Cmd {
    sealed interface Controls: AnimationTimelineCmd {
        object Play: Controls, () -> Play {
            override fun invoke(): Play = this
        }
    }
}

data class AnimationScreenState(
    val current: String,
    val sprites: List<AnimSprite>,
    override val id: String = UUID.randomUUID().toString(),
): ScreenState {
    data class AnimSprite(
        val id: String = UUID.randomUUID().toString(),
        val region: AtlasData.Region,
        val duration: Long = 0,
        val x: Int = 0,
        val y: Int = 0,
    )
}
