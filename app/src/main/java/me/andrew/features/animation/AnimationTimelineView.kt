package me.andrew.features.animation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import me.andrew.Cmd
import me.andrew.utils.rememberListener

@Composable
fun AnimationTimelineView(
    modifier: Modifier,
    state: AnimationScreenState,
    dispatch: (Cmd) -> Unit,
) = Row(modifier = modifier) {
    AnimControllers(dispatch = dispatch)
    Timeline(
        modifier = Modifier.Companion.weight(1f).fillMaxHeight(),
        sprites = state.sprites,
        //dispatch = dispatch,
    )
}

@Composable
private fun Timeline(
    modifier: Modifier,
    sprites: List<AnimationScreenState.AnimSprite>,
    //dispatch: (Cmd) -> Unit,
) {
    LazyRow(modifier = modifier) {
        items(sprites) {
            //SpriteView(data = it.region)
        }
    }
}

@Composable
private fun AnimControllers(
    modifier: Modifier = Modifier,
    dispatch: (Cmd) -> Unit,
) = Column(modifier = modifier) {
    Button(onClick = dispatch.rememberListener(AnimationTimelineCmd.Controls.Play)) {

    }
}
