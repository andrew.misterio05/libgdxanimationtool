package me.andrew.features.app

import me.andrew.Cmd

sealed interface AppCmd: Cmd {
    sealed interface NewScreen : AppCmd {
        data class Workspace(val atlasPath: String): NewScreen
    }
    object CloseScreen : AppCmd

    data class ScreenCmd(val value: Cmd): AppCmd
}
