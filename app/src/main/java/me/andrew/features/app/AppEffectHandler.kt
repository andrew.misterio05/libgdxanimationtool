package me.andrew.features.app

import kotlinx.coroutines.CoroutineScope
import me.andrew.Cmd
import me.andrew.Eff
import me.andrew.EffHandler
import me.andrew.EffHandler.Companion.execute
import me.andrew.features.app.navigation.NavigationEff
import me.andrew.features.app.navigation.NavigationEffectHandler
import me.andrew.features.onboarding.OnboardingFeatureEff
import me.andrew.features.onboarding.OnboardingFeatureEffectHandler
import me.andrew.features.workspace.WorkSpaceEffHandler
import me.andrew.features.workspace.WorkspaceFeatureEff

class AppEffectHandler(
    private val navigationEffectHandler: NavigationEffectHandler,
    private val onboardingFeatureEffectHandler: OnboardingFeatureEffectHandler,
    private val worksEffHandler: WorkSpaceEffHandler,
) : EffHandler<Eff> {

    override suspend fun CoroutineScope.execute(eff: Eff, dispatch: (Cmd) -> Unit) {
        when (eff) {
            is NavigationEff -> navigationEffectHandler.execute(this, eff, dispatch)
            is OnboardingFeatureEff -> onboardingFeatureEffectHandler.execute(this, eff, dispatch)
            is WorkspaceFeatureEff -> worksEffHandler.execute(this, eff, dispatch)
        }
    }
}
