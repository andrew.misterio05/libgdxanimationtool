package me.andrew.features.app

import arrow.core.andThen
import kotlinx.collections.immutable.persistentListOf
import kotlinx.coroutines.supervisorScope
import me.andrew.Cmd
import me.andrew.Eff
import me.andrew.EffHandler
import me.andrew.features.onboarding.OnboardingScreenState
import me.andrew.screens.base.BaseViewModel
import org.orbitmvi.orbit.syntax.simple.reduce

private fun initialState(path: String?) = AppState(
    screens = persistentListOf(
        OnboardingScreenState(
            createNew = "Create",
            openSome = "Open",
            openFileDialog = path?.let {
                OnboardingScreenState.OpenFileDialog(
                    title = "TODO",
                    initPath = path,
                    type = it.split(".").lastOrNull().orEmpty(),
                )
            }
        ),
    )
)

class AppFeature(
    argumentPath: String? = null,
    private val reducer: AppReducer,
    private val effHandler: EffHandler<Eff>,
    private val cmdMapper: (Cmd) -> Cmd = { it },
) : BaseViewModel<AppState, Nothing>(initialState(argumentPath)) {

    fun dispatch(cmd: Cmd) = intent {
        val result = reducer(state, cmd.let(cmdMapper andThen ::mapCmd))
        reduce { result.first }
        result.second.forEach { eff -> onSideEffect(eff) }
    }

    private suspend fun onSideEffect(effect: Eff) {
        supervisorScope {
            effHandler.run {
                execute(eff = effect, dispatch = ::mapCmd andThen ::dispatch)
            }
        }
    }

    private fun mapCmd(cmd: Cmd) = when (cmd) {
        is AppCmd -> cmd
        else -> AppCmd.ScreenCmd(cmd)
    }
}
