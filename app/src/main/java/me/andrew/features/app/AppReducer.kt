package me.andrew.features.app

import java.io.File
import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.plus
import me.andrew.AtlasData
import me.andrew.Cmd
import me.andrew.Eff
import me.andrew.Reducer
import me.andrew.ReducerResult
import me.andrew.features.ScreenState
import me.andrew.features.atlas.AtlasFeatureCmd
import me.andrew.features.atlas.AtlasFeatureEff
import me.andrew.features.atlas.AtlasScreenState
import me.andrew.features.onboarding.OnboardingFeatureCmd
import me.andrew.features.onboarding.OnboardingFeatureEff
import me.andrew.features.onboarding.OnboardingScreenState
import me.andrew.features.workspace.WorkspaceFeatureCmd
import me.andrew.features.workspace.WorkspaceFeatureEff
import me.andrew.features.workspace.WorkspaceScreenState
import me.andrew.utils.dropLast

class AppReducer(
    private val emptyScreenReducer: Reducer<OnboardingScreenState, OnboardingFeatureCmd, OnboardingFeatureEff>,
    private val workspaceScreenReducer: Reducer<WorkspaceScreenState, WorkspaceFeatureCmd, WorkspaceFeatureEff>,
    private val atlasScreenReducer: Reducer<AtlasScreenState, AtlasFeatureCmd, AtlasFeatureEff>,
) : Reducer<AppState, Cmd, Eff> {

    override fun invoke(state: AppState, cmd: Cmd): ReducerResult<AppState, Eff> {
        val emptyResult by lazy { Reducer.result(state) }

        return when (cmd) {
            is AppCmd -> when (cmd) {
                is AppCmd.ScreenCmd -> state.reduceByScreen(state.currentScreen, cmd) ?: emptyResult
                is AppCmd.CloseScreen -> Reducer.result(state.copy(screens = state.screens.dropLast(1)))
                is AppCmd.NewScreen -> when (cmd) {
                    is AppCmd.NewScreen.Workspace -> {
                        val data = AtlasData(File(cmd.atlasPath))
                        Reducer.result(state.copy(screens = state.screens + WorkspaceScreenState.create(data)))
                    }
                }
            }

            else -> emptyResult
        }
    }

    private fun AppState.reduceByScreen(
        screen: ScreenState?,
        cmd: AppCmd.ScreenCmd,
        updateScreen: AppState.(ScreenState, ScreenState) -> AppState = { new, old ->
            when (new) {
                old -> this
                else -> copy(screens = screens.change(new = new, old = old))
            }
        }
    ): ReducerResult<AppState, Eff>? {
        fun <S : ScreenState, C : Cmd> Reducer<S, C, out Eff>.execute(screen: S, cmd: C): ReducerResult<AppState, Eff> {
            val screenResult = this(screen, cmd)
            return Reducer.result(updateScreen(screenResult.first, screen), screenResult.second)
        }
        return when {
            screen is OnboardingScreenState && cmd.value is OnboardingFeatureCmd -> {
                emptyScreenReducer.execute(screen, cmd.value)
            }

            screen is WorkspaceScreenState -> when (cmd.value) {
                is WorkspaceFeatureCmd -> {
                    workspaceScreenReducer.execute(screen, cmd.value)
                }

                is AtlasFeatureCmd -> reduceByScreen(screen = screen.atlasScreenData, cmd = cmd) { new, _ ->
                    updateScreen(screen.copy(atlasScreenData = new), screen)
                }

                else -> null
            }

            screen is AtlasScreenState && cmd.value is AtlasFeatureCmd -> {
                atlasScreenReducer.execute(screen, cmd.value)
            }

            else -> null
        }
    }

    private fun <T> PersistentList<T>.change(new: T, old: T): PersistentList<T> {
        val index = indexOf(old)
        return when {
            index >= 0 -> set(index, new)
            else -> this
        }
    }
}
