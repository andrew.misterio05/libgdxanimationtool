package me.andrew.features.app

import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.persistentListOf
import me.andrew.features.ScreenState

data class AppState(
    val popup: ScreenState? = null,
    val screens: PersistentList<ScreenState> = persistentListOf(),
) {
    val currentScreen get() = screens.lastOrNull()
}
