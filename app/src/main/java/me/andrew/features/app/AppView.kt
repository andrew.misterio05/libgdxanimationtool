package me.andrew.features.app

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.updateTransition
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.with
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import me.andrew.Cmd
import me.andrew.features.ScreenState
import me.andrew.features.animation.AnimationScreenState
import me.andrew.features.animation.AnimationTimelineView
import me.andrew.features.atlas.AtlasScreenState
import me.andrew.features.atlas.AtlasView
import me.andrew.features.onboarding.OnboardingScreenState
import me.andrew.features.onboarding.OnboardingView
import me.andrew.features.workspace.WorkspaceScreenState
import me.andrew.features.workspace.WorkspaceView

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun AppView(
    modifier: Modifier = Modifier,
    state: AppState,
    dispatch: (Cmd) -> Unit,
) {
    val transition = updateTransition(targetState = state.currentScreen ?: return)
    transition.AnimatedContent(
        modifier = modifier,
        contentKey = ScreenState::id,
        transitionSpec = {
            slideInHorizontally(initialOffsetX = { it / 2 }) + fadeIn() with slideOutHorizontally() + fadeOut()
        },
    ) { screen ->
        when (screen) {
            is OnboardingScreenState -> OnboardingView(Modifier.fillMaxSize(), screen, dispatch)
            is WorkspaceScreenState -> {
                WorkspaceView(
                    modifier = Modifier.fillMaxSize(),
                    state = screen,
                    dispatch = dispatch,
                    atlasView = { modifier, screenState, dispatch ->
                        AtlasView(modifier, screenState as AtlasScreenState, dispatch)
                    },
                    editorScreen = { modifier, screenState, dispatch ->
                        AnimationTimelineView(modifier, screenState as AnimationScreenState, dispatch)
                    },
                )
            }
        }
    }
}
