package me.andrew.features.app

import me.andrew.Cmd

class CmdMapper(
    private val workspaceCmdMapper: (Cmd) -> Cmd?,
) {

    operator fun invoke(cmd: Cmd): Cmd = workspaceCmdMapper(cmd)
        ?: cmd
}
