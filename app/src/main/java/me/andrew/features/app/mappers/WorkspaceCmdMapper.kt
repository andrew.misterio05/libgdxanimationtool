package me.andrew.features.app.mappers

import me.andrew.Cmd
import me.andrew.features.atlas.AtlasFeatureCmd

class WorkspaceCmdMapper {
    operator fun invoke(cmd: Cmd): Cmd? = when(cmd) {
        is AtlasFeatureCmd.Regions.OnRegionApply -> null
        else -> null
    }
}
