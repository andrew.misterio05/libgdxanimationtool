package me.andrew.features.app.navigation

import me.andrew.Eff
import me.andrew.features.ScreenState

sealed interface NavigationEff : Eff {
    object Back : NavigationEff
}
