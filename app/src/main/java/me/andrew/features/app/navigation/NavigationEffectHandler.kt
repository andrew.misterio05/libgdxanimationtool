package me.andrew.features.app.navigation

import kotlinx.coroutines.CoroutineScope
import me.andrew.Cmd
import me.andrew.EffHandler
import me.andrew.features.app.AppCmd

class NavigationEffectHandler : EffHandler<NavigationEff> {
    override suspend fun CoroutineScope.execute(eff: NavigationEff, dispatch: (Cmd) -> Unit) = when (eff) {
        is NavigationEff.Back -> dispatch(AppCmd.CloseScreen)
    }
}
