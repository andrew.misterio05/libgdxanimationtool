package me.andrew.features.atlas

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import java.util.UUID
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toImmutableList
import me.andrew.AtlasData
import me.andrew.Cmd
import me.andrew.Eff
import me.andrew.Reducer
import me.andrew.features.ScreenState
import me.andrew.utils.minus
import me.andrew.utils.plus

@JvmInline
value class SpriteName(val value: String)

@Stable
data class AtlasScreenState(
    val regions: ImmutableList<Region>,
    val selected: ImmutableList<SpriteName> = persistentListOf(),
    override val id: String = UUID.randomUUID().toString(),
) : ScreenState {
    @Immutable
    data class Region(
        val name: SpriteName,
        val pageFilePath: String,
        val srcSize: IntSize,
        val srcOffset: IntOffset,
    )

    companion object {
        fun create(atlasData: AtlasData) = AtlasScreenState(
            regions = atlasData.regions.map {
                Region(
                    name = SpriteName(it.name),
                    pageFilePath = it.page.textureFile.path,
                    srcSize = IntSize(it.width, it.height),
                    srcOffset = IntOffset(it.left, it.top),
                )
            }
                .toImmutableList()
        )
    }
}

sealed interface AtlasFeatureCmd : Cmd {
    sealed interface Regions : AtlasFeatureCmd {
        data class OnRegionSelected(val region: AtlasScreenState.Region) : Regions
        data class OnRegionApply(val region: AtlasScreenState.Region) : Regions
    }
}

sealed interface AtlasFeatureEff : Eff

val AtlasScreenReducer = Reducer<AtlasScreenState, AtlasFeatureCmd, AtlasFeatureEff> { state, cmd ->
    when (cmd) {
        is AtlasFeatureCmd.Regions.OnRegionApply -> error("This command should have been redirected")
        is AtlasFeatureCmd.Regions.OnRegionSelected -> {
            Reducer.result(
                state.copy(
                    selected = when (cmd.region.name) {
                        in state.selected -> state.selected - cmd.region.name
                        else -> state.selected + cmd.region.name
                    }
                )
            )
        }
    }
}
