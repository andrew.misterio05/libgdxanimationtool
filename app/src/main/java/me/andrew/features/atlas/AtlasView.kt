package me.andrew.features.atlas

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.text.font.FontWeight
import arrow.core.andThen
import kotlinx.collections.immutable.ImmutableList
import me.andrew.Cmd
import me.andrew.features.spriteview.SpriteScaleType
import me.andrew.features.spriteview.SpriteView
import me.andrew.ui.theme.dimens
import me.andrew.utils.loadImage

private const val MIN_COLUMNS_COUNT = 3
private const val CELL_WIDTH = 300

@Composable
fun AtlasView(
    modifier: Modifier,
    state: AtlasScreenState,
    dispatch: (Cmd) -> Unit,
) {
    RegionsView(
        modifier = modifier.fillMaxSize(),
        regions = state.regions,
        selected = state.selected,
        onClick = remember { AtlasFeatureCmd.Regions::OnRegionSelected andThen dispatch },
    )
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun RegionsView(
    modifier: Modifier,
    regions: ImmutableList<AtlasScreenState.Region>,
    selected: ImmutableList<SpriteName>,
    onClick: (AtlasScreenState.Region) -> Unit,
) {
    var width by remember { mutableStateOf(0) }
    val columns by remember { derivedStateOf { (width / CELL_WIDTH).coerceAtLeast(MIN_COLUMNS_COUNT) } }
    val loadedPages = remember { mutableMapOf<String, ImageBitmap>() }
    val paddings = MaterialTheme.dimens.small
    LazyVerticalGrid(
        columns = GridCells.Fixed(columns),
        modifier = modifier.onSizeChanged { width = it.width },
        contentPadding = PaddingValues(
            bottom = paddings,
            start = paddings,
            end = paddings,
        ),
    ) {
        items(regions) { item ->
            ItemView(onClick, item, loadedPages, selected)
        }
    }
}

@Composable
private fun ItemView(
    onClick: (AtlasScreenState.Region) -> Unit,
    item: AtlasScreenState.Region,
    loadedPages: MutableMap<String, ImageBitmap>,
    selected: ImmutableList<SpriteName>
) {
    val paddings = MaterialTheme.dimens.small
    val halfSmallPadding = paddings / 2
    val shape = MaterialTheme.shapes.medium
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(1f)
            .padding(top = paddings, start = halfSmallPadding, end = halfSmallPadding)
            .background(color = MaterialTheme.colors.surface, shape = shape)
            .clip(shape)
            .clickable { onClick(item) },
    ) {
        val path = item.pageFilePath
        SpriteView(
            modifier = Modifier.matchParentSize().padding(paddings),
            imageBitmap = loadedPages.getOrPut(path) { loadImage(path) },
            srcOffset = item.srcOffset,
            srcSize = item.srcSize,
            scaleType = SpriteScaleType.FIT,
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colors.surface)
                .align(Alignment.BottomCenter)
                .padding(paddings),
            text = item.name.value,
            maxLines = 1,
            color = MaterialTheme.colors.onSurface,
        )

        SelectionMarkerView(
            modifier = Modifier
                .fillMaxSize(),
            itemIndex = selected.indexOf(item.name),
            shape = shape,
        )
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun SelectionMarkerView(
    modifier: Modifier = Modifier,
    itemIndex: Int,
    shape: CornerBasedShape
) = AnimatedVisibility(
    modifier = modifier,
    visible = itemIndex >= 0,
    enter = fadeIn(),
    exit = fadeOut(),
) {
    val paddings = MaterialTheme.dimens.small
    val accentColor = MaterialTheme.colors.onSurface
    Box(
        modifier = Modifier
            .fillMaxSize()
            .border(
                width = paddings / 2,
                color = accentColor,
                shape = shape,
            ),
    ) {

        AnimatedContent(
            modifier = Modifier.padding(paddings).align(Alignment.TopEnd),
            targetState = itemIndex.takeIf { it >= 0 }?.toString().orEmpty(),
            transitionSpec = {
                (slideInVertically(initialOffsetY = { it / 2 }) + fadeIn())
                    .with(slideOutVertically() + fadeOut())
            }
        ) {
            Text(
                text = it,
                color = accentColor,
                fontWeight = FontWeight.Bold,
            )
        }
    }
}
