package me.andrew.features.onboarding

import me.andrew.Cmd

sealed interface OnboardingFeatureCmd : Cmd {
    sealed interface ShowOpenFileDialog : OnboardingFeatureCmd {
        object New : ShowOpenFileDialog
        object Recent : ShowOpenFileDialog
    }

    object HideOpenFileDialog : OnboardingFeatureCmd
    data class OnFileSelected(val path: String) : OnboardingFeatureCmd
}
