package me.andrew.features.onboarding

import me.andrew.Eff

sealed interface OnboardingFeatureEff : Eff {
    data class CreateNewAirFile(val airPath: String, val atlasPath: String) : OnboardingFeatureEff
}
