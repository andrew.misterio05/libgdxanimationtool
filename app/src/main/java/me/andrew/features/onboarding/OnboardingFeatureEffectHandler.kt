package me.andrew.features.onboarding

import kotlinx.coroutines.CoroutineScope
import me.andrew.Cmd
import me.andrew.EffHandler
import me.andrew.features.app.AppCmd

class OnboardingFeatureEffectHandler : EffHandler<OnboardingFeatureEff> {

    override suspend fun CoroutineScope.execute(eff: OnboardingFeatureEff, dispatch: (Cmd) -> Unit) = when (eff) {
        is OnboardingFeatureEff.CreateNewAirFile -> dispatch(AppCmd.NewScreen.Workspace(atlasPath = eff.atlasPath))
    }
}
