package me.andrew.features.onboarding

import java.io.File
import me.andrew.Reducer
import me.andrew.ReducerResult


private const val ATLAS_FILE_TYPE = "atlas"
private const val ANIMATION_FILE_TYPE = "air"

val OnboardingScreenReducer = Reducer<OnboardingScreenState, OnboardingFeatureCmd, OnboardingFeatureEff> { state, cmd ->
    when (cmd) {
        is OnboardingFeatureCmd.HideOpenFileDialog -> Reducer.result(state.copy(openFileDialog = null))
        is OnboardingFeatureCmd.OnFileSelected -> onFileSelected(state, cmd)
        is OnboardingFeatureCmd.ShowOpenFileDialog.New -> showDialog(state, ATLAS_FILE_TYPE)
        is OnboardingFeatureCmd.ShowOpenFileDialog.Recent -> showDialog(state, ANIMATION_FILE_TYPE)
    }
}

private fun onFileSelected(
    state: OnboardingScreenState,
    cmd: OnboardingFeatureCmd.OnFileSelected
): ReducerResult<OnboardingScreenState, OnboardingFeatureEff> {
    val resultEffect = OnboardingFeatureEff.CreateNewAirFile(
        airPath = cmd.path.substringBeforeLast(File.separator)
            .plus(File.separator + cmd.path.substringAfterLast(File.separator).substringBeforeLast("."))
            .plus(".")
            .plus(ANIMATION_FILE_TYPE),
        atlasPath = cmd.path,
    )
    return Reducer.result(state, resultEffect)
}

private fun showDialog(state: OnboardingScreenState, type: String) = Reducer.result(
    state = state.copy(
        openFileDialog = OnboardingScreenState.OpenFileDialog(
            title = "Chose atlas file",
            initPath = System.getenv("initPath") ?: System.getProperty("user.home"),
            type = type,
        )
    )
)


