package me.andrew.features.onboarding

import java.util.UUID
import me.andrew.features.ScreenState

data class OnboardingScreenState(
    val openFileDialog: OpenFileDialog? = null,
    val createNew: String,
    val openSome: String,
    override val id: String = UUID.randomUUID().toString(),
) : ScreenState {

    data class OpenFileDialog(
        val title: String,
        val initPath: String,
        val type: String,
    )
}
