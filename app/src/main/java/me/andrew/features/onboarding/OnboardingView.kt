package me.andrew.features.onboarding

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import arrow.core.andThen
import me.andrew.Cmd
import me.andrew.design.AATextButton
import me.andrew.design.launchFileDialog

@Composable
fun OnboardingView(
    modifier: Modifier,
    onboardingScreenState: OnboardingScreenState,
    dispatch: (Cmd) -> Unit,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        AATextButton(
            text = onboardingScreenState.createNew,
            onClick = { dispatch(OnboardingFeatureCmd.ShowOpenFileDialog.New) },
        )
        Spacer(Modifier.size(16.dp))
        AATextButton(
            text = onboardingScreenState.openSome,
            onClick = { dispatch(OnboardingFeatureCmd.ShowOpenFileDialog.Recent) },
        )
    }

    dialog(onboardingScreenState.openFileDialog, dispatch)
}

@Composable
private fun dialog(
    openFileDialog: OnboardingScreenState.OpenFileDialog?,
    dispatch: (Cmd) -> Unit,
) {
    val coroutine = rememberCoroutineScope()
    LaunchedEffect(openFileDialog) {
        openFileDialog?.let { dialog ->
            kotlin.runCatching {
                coroutine.launchFileDialog(
                    title = dialog.title,
                    initPath = dialog.initPath,
                    type = openFileDialog.type,
                )
                    .await()
            }
                .mapCatching { if (it.isNullOrEmpty()) error("File has not opened!") else it }
                .fold(
                    onSuccess = OnboardingFeatureCmd::OnFileSelected andThen dispatch,
                    onFailure = { dispatch(OnboardingFeatureCmd.HideOpenFileDialog) },
                )
        }
    }
}
