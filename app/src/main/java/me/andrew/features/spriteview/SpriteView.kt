package me.andrew.features.spriteview

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize

@Composable
fun SpriteView(
    modifier: Modifier = Modifier,
    scaleType: SpriteScaleType = SpriteScaleType.WH,
    imageBitmap: ImageBitmap,
    srcSize: IntSize,
    srcOffset: IntOffset,
    offset: IntOffset = IntOffset.Zero,
) {
    var size by remember { mutableStateOf(IntSize(srcSize.width, srcSize.height)) }

    val (scaleX, scaleY) = when (scaleType) {
        SpriteScaleType.WH -> size.width.toFloat() / srcSize.width to size.height.toFloat() / srcSize.height
        SpriteScaleType.SRC -> {
            Pair(
                first = (size.width.toFloat() / srcSize.width).coerceAtLeast(1f),
                second = (size.height.toFloat() / srcSize.height).coerceAtLeast(1f)
            )
        }
        SpriteScaleType.FIT -> {
            val ratio = srcSize.run { width.toFloat() / height }
            val (w, h) = size.width.toFloat() / srcSize.width to size.height.toFloat() / srcSize.height
            Pair(
                first = when {
                    ratio > 1 -> w
                    else -> w * ratio
                },
                second = when {
                    ratio > 1 -> h / ratio
                    else -> h
                },
            )
        }
    }
    Canvas(
        modifier
            .onSizeChanged { size = it }
            .scale(
                scaleY = scaleY,
                scaleX = scaleX,
            ),
    ) {
        drawImage(
            image = imageBitmap,
            srcSize = srcSize,
            srcOffset = srcOffset,
            dstOffset = IntOffset(
                x = (size.width - srcSize.width) / 2 + offset.x,
                y = (size.height - srcSize.height) / 2 + offset.y,
            ),
        )
    }
}

enum class SpriteScaleType {
    WH,
    SRC,
    FIT,
    ;
}
