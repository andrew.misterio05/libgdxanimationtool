package me.andrew.features.spriteview

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize

data class SpriteViewData(
    val imageBitmap: ImageBitmap,
    val srcSize: IntSize,
    val srcOffset: IntOffset,
    val offset: IntOffset,
)
