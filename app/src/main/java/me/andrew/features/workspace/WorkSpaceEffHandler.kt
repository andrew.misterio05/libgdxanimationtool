package me.andrew.features.workspace

import kotlinx.coroutines.CoroutineScope
import me.andrew.Cmd
import me.andrew.EffHandler

class WorkSpaceEffHandler : EffHandler<WorkspaceFeatureEff> {
    override suspend fun CoroutineScope.execute(eff: WorkspaceFeatureEff, dispatch: (Cmd) -> Unit) {
        // Do nothing, todo
    }
}
