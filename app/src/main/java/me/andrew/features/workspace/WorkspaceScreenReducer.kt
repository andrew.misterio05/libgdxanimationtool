package me.andrew.features.workspace

import me.andrew.Cmd
import me.andrew.Eff
import me.andrew.Reducer

sealed interface WorkspaceFeatureCmd : Cmd
sealed interface WorkspaceFeatureEff : Eff

val WorkspaceScreenReducer = Reducer<WorkspaceScreenState, WorkspaceFeatureCmd, WorkspaceFeatureEff> {
    state, cmd -> when (cmd) {
        else -> Reducer.result(state)
    }
}
