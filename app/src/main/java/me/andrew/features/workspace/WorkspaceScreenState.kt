package me.andrew.features.workspace

import java.util.UUID
import me.andrew.AtlasData
import me.andrew.features.ScreenState
import me.andrew.features.atlas.AtlasScreenState

data class WorkspaceScreenState internal constructor(
    val atlasScreenData: ScreenState,
    val animationsScreenData: ScreenState,
    val animationEditorScreenData: ScreenState?,
    override val id: String = UUID.randomUUID().toString(),
) : ScreenState {
    companion object {
        fun create(data: AtlasData) = WorkspaceScreenState(
            atlasScreenData = AtlasScreenState.create(atlasData = data),
            animationsScreenData = ScreenState,
            animationEditorScreenData = null,
        )
    }
}
