package me.andrew.features.workspace

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import me.andrew.Cmd
import me.andrew.features.ScreenState

@Composable
fun WorkspaceView(
    modifier: Modifier = Modifier,
    state: WorkspaceScreenState,
    dispatch: (Cmd) -> Unit,
    atlasView: @Composable (Modifier, ScreenState, (Cmd) -> Unit) -> Unit,
    editorScreen: @Composable (Modifier, ScreenState, (Cmd) -> Unit) -> Unit,
) {
    // Список регионов будет открываться только при добавления спрайта в анимацию и будет как попап или новый экран
    // Возможно лучше экраном с возможностью множественного выбора
    // Так же замена спрайта анимации но с доп инфой о том какой текущий был спрайт(выделение его в списке)
    Row(modifier.animateContentSize()) {
        atlasView(Modifier.fillMaxHeight().weight(1f), state.atlasScreenData, dispatch)
        state.animationEditorScreenData?.let { editorScreen ->
            editorScreen(Modifier.fillMaxHeight().weight(2f), editorScreen, dispatch)
        }
    }
}
