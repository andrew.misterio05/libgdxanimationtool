package me.andrew

import androidx.compose.foundation.LocalScrollbarStyle
import androidx.compose.foundation.ScrollbarStyle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.singleWindowApplication
import me.andrew.features.app.AppEffectHandler
import me.andrew.features.app.AppFeature
import me.andrew.features.app.AppReducer
import me.andrew.features.app.AppView
import me.andrew.features.app.CmdMapper
import me.andrew.features.app.navigation.NavigationEffectHandler
import me.andrew.features.atlas.AtlasScreenReducer
import me.andrew.features.onboarding.OnboardingFeatureEffectHandler
import me.andrew.features.onboarding.OnboardingScreenReducer
import me.andrew.features.workspace.WorkSpaceCmdMapper
import me.andrew.features.workspace.WorkSpaceEffHandler
import me.andrew.features.workspace.WorkspaceScreenReducer
import me.andrew.ui.theme.AppTheme
import me.andrew.utils.Logger

fun main() = singleWindowApplication(
    title = "AtlasAnimator",
    content = {
        Logger.initLogCore(::println)
        val appFeature = remember {
            val navigationEffectHandler = NavigationEffectHandler()
            val cmdMapper = CmdMapper(
                workspaceCmdMapper = WorkSpaceCmdMapper,
            )
            AppFeature(
                reducer = AppReducer(
                    emptyScreenReducer = OnboardingScreenReducer,
                    workspaceScreenReducer = WorkspaceScreenReducer,
                    atlasScreenReducer = AtlasScreenReducer,
                ),
                effHandler = AppEffectHandler(
                    navigationEffectHandler = navigationEffectHandler,
                    onboardingFeatureEffectHandler = OnboardingFeatureEffectHandler(),
                    worksEffHandler = WorkSpaceEffHandler(),
                ),
                cmdMapper = cmdMapper::invoke
            )
        }
        MainContainer {
            val state by appFeature.state.collectAsState()
            AppView(
                modifier = Modifier.fillMaxSize().background(MaterialTheme.colors.background),
                state = state,
                dispatch = appFeature::dispatch,
            )
        }
    }
)

@Composable
private fun MainContainer(content: @Composable () -> Unit) = AppTheme(isDark = true) {
    CompositionLocalProvider(
        LocalScrollbarStyle provides ScrollbarStyle(
            minimalHeight = 16.dp,
            thickness = 8.dp,
            shape = MaterialTheme.shapes.small,
            hoverDurationMillis = 300,
            unhoverColor = MaterialTheme.colors.onSurface.copy(alpha = 0.12f),
            hoverColor = MaterialTheme.colors.onSurface.copy(alpha = 0.50f)
        ),
        content = content
    )
}
