package me.andrew.models

import androidx.compose.ui.graphics.ImageBitmap

typealias ImagePack = Map<String, ImageBitmap>
