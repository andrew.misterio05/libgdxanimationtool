package ui.resources

class Resources(
    private val strings: (Text) -> String,
) {
    operator fun <T> get(key: ResKey<T>): T? = when (key) {
        is Text -> strings(key) as? T
        else -> null
    }
}
