package ui.resources

enum class Text: ResKey<String> {
    PLAY,
    STOP,
    REMOVE,
    CLEAR,
    ;
}
