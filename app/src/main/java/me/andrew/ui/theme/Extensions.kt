package me.andrew.ui.theme

import androidx.compose.runtime.Composable
import ui.resources.Text

@Composable
fun localText(key: Text): String = LocalResources.current[key].orEmpty()
