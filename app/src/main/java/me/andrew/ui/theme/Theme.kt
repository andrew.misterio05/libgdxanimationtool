package me.andrew.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import me.andrew.ui.theme.resources.Dimens
import ui.resources.Resources
import ui.theme.resources.enStrings
import ui.theme.shapes
import ui.theme.typography

private val darkColorPalette = darkColors(
    primary = Color(0xff00796B),
    primaryVariant = Color(0xff009688),

    secondary = Color(0xff388E3C),
    onPrimary = Color(0xffE8E8E8),
    onSecondary = Color.White,
    background = Color(0xFF121212),
    surface = Color(0xff757575),
    error = Color(0xffD32F2F),
)

private val lightColorPalette = lightColors(
    primary = Color(0xffB2DFDB),
    primaryVariant = Color(0xff009688),

    secondary = Color(0xff4CAF50),
    onPrimary = Color(0xff212121),
    onSecondary = Color.Black,
    background = Color(0xFFEDEDED),
    surface = Color(0xffBDBDBD),
    error = Color(0xffF44336),
)

private val resources = Resources(
    strings = enStrings,
)
private val dimens = Dimens(
    small = 8.dp,
    medium = 16.dp,
    large = 32.dp,
)
val LocalResources = compositionLocalOf { resources }
val LocalDimens = compositionLocalOf { dimens }

val MaterialTheme.dimens @Composable get() = LocalDimens.current

@Composable
fun AppTheme(
    isDark: Boolean,
    content: @Composable () -> Unit,
) = CompositionLocalProvider(
    LocalResources provides resources,
    LocalDimens provides dimens,
) {
    MaterialTheme(
        colors = when {
            isDark -> darkColorPalette
            else -> lightColorPalette
        },
        typography = typography,
        shapes = shapes,
        content = content
    )
}
