package me.andrew.ui.theme.resources

import androidx.compose.ui.unit.Dp

data class Dimens(
    val small: Dp,
    val medium: Dp,
    val large: Dp,
)
