package ui.theme.resources

import ui.resources.Text

internal val enStrings: (Text) -> String = Text::name
