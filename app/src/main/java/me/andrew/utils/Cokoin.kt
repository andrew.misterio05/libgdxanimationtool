package me.andrew.utils


import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import org.koin.core.Koin
import org.koin.core.KoinApplication
import org.koin.core.annotation.KoinInternalApi
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.core.scope.Scope
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.ModuleDeclaration
import org.koin.dsl.koinApplication
import org.koin.java.KoinJavaComponent.getKoin
import org.koin.mp.KoinPlatformTools

val koin: Koin @Composable get() = LocalKoin.current

val scope @Composable get() = LocalScope.current

@PublishedApi
internal val LocalKoin = compositionLocalOf { getKoin() }

@PublishedApi
internal val LocalScope = compositionLocalOf {
    @OptIn(KoinInternalApi::class)
    getKoin().scopeRegistry.rootScope
}

@Composable
inline fun <reified T> get(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null,
): T {
    val scope = scope
    return remember(qualifier, parameters) {
        scope.get(qualifier, parameters)
    }
}

@Composable
fun KoinScope(
    qualifier: Qualifier,
    content: @Composable () -> Unit
) = KoinScope(
    getScope = { createScope(scopeId = KoinPlatformTools.generateId(), qualifier = qualifier) },
    content = content
)

@Composable
fun KoinScope(
    getScope: Koin.() -> Scope,
    content: @Composable () -> Unit
) {
    val currentScope = LocalScope.current
    val koin = getKoin()
    val scope = remember {
        koin.getScope().also {
            it.linkTo(currentScope)
        }
    }
    CompositionLocalProvider(LocalScope provides scope) {
        content()
    }
}

@Composable
fun Koin(
    koinApplication: KoinApplication,
    content: @Composable () -> Unit,
) {
    @OptIn(KoinInternalApi::class)
    val rootScope = koinApplication.koin.scopeRegistry.rootScope
    CompositionLocalProvider(
        LocalKoin provides koinApplication.koin,
        LocalScope provides rootScope
    ) {
        content()
    }
}

@Composable
fun Koin(
    appDeclaration: KoinAppDeclaration? = null,
    content: @Composable () -> Unit,
) = Koin(
    koinApplication = koinApplication(appDeclaration),
    content = content
)
