package me.andrew.utils

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import me.andrew.Cmd

@Composable
fun ((Cmd) -> Unit).rememberListener(getCmd: () -> Cmd): () -> Unit = remember(this, getCmd) { { invoke(getCmd()) } }
