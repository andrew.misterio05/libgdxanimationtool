package me.andrew.utils

import androidx.compose.runtime.Composable

fun <P1, P2, P3, R> (@Composable (P1, P2, P3) -> R).partially2(p2: P2): @Composable (P1, P3) -> R = { p1, p3 ->
    this(p1, p2, p3)
}

fun <P1, P2, P3, R> (@Composable (P1, P2, P3) -> R).partially1(p1: P1): @Composable (P2, P3) -> R = { p2, p3 ->
    this(p1, p2, p3)
}

fun <P1, P2, P3, R> (@Composable (P1, P2, P3) -> R).partially3(p3: P3): @Composable (P1, P2) -> R = { p1, p2 ->
    this(p1, p2, p3)
}
