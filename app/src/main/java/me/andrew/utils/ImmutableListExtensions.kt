package me.andrew.utils

import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.toImmutableList
import kotlin.collections.map
import kotlin.collections.plus
import kotlin.collections.minus
import kotlin.collections.dropLast
import kotlinx.collections.immutable.toPersistentList

fun <T, R> ImmutableList<T>.map(mapper: (T) -> R): ImmutableList<R> = map(mapper).toImmutableList()

operator fun <T> ImmutableList<T>.plus(other: T): ImmutableList<T> = (this + other).toImmutableList()

operator fun <T> ImmutableList<T>.minus(other: T): ImmutableList<T> = (this - other).toImmutableList()

fun <T> PersistentList<T>.dropLast(count: Int): PersistentList<T> = dropLast(count).toPersistentList()

