package me.andrew.utils

object Logger {

    private var logCore: (String) -> Unit = {}

    fun log(text: String) = logCore(text)

    fun initLogCore(log: (String) -> Unit) {
        logCore = log
    }
}
