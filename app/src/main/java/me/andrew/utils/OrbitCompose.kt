package utils

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@Composable
fun <T : Any> Flow<T>.collectEvents(
    onEvent: (suspend (event: T) -> Unit)
) = LaunchedEffect(this) {
    coroutineScope {
        launch {
            this@collectEvents.collect(onEvent)
        }
    }
}
