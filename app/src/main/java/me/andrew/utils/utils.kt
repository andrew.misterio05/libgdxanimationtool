package me.andrew.utils

import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import arrow.core.andThen
import java.awt.Cursor
import java.io.File
import me.andrew.AtlasData
import me.andrew.AtlasFormat
import me.andrew.models.ImagePack
import org.jetbrains.skia.Image

fun getAtlasPath(args: Array<out String>) = args
    .getOrNull(0)
    ?.takeIf { it.endsWith(AtlasFormat) }
    ?: "G:\\projects\\Wizards\\sources\\platforms\\android\\src\\main\\assets\\units\\castle\\pikeman\\pikeman.atlas"

fun loadImage(path: String): ImageBitmap = loadImage(File(path))
fun loadImage(file: File): ImageBitmap = Image.makeFromEncoded(file.readBytes()).toComposeImageBitmap()

fun Modifier.cursor(awtCursorId: Int) = pointerHoverIcon(PointerIcon(Cursor(awtCursorId)))

operator fun ImagePack.get(region: AtlasData.Region, fileName: String): ImageBitmap =
    requireNotNull(get(fileName)) {
        "Image for region:${region.name} not found!"
    }

operator fun List<AtlasData.Region>.get(name: String): AtlasData.Region =
    first(AtlasData.Region::name andThen name::equals)
