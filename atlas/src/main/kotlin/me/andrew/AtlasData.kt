@file:Suppress(
    "MaximumLineLength",
    "MagicNumber",
    "LoopWithTooManyJumpStatements",
    "TooGenericExceptionThrown",
    "TooGenericExceptionCaught",
    "FunctionParameterNaming",
    "ReturnCount",
)

package me.andrew

import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import me.andrew.AtlasData.Field

class AtlasData @JvmOverloads constructor(
    packFile: File,
    imagesDir: File = packFile.parentFile,
) {
    val pages: List<Page>
    val regions: List<Region>

    init {
        val regions = ArrayList<Region>()
        val pages = ArrayList<Page>()
        val entry = arrayOfNulls<String>(5)
        val pageFields: MutableMap<String?, Field<Page>> = HashMap(15, 0.99f) // Size needed to avoid collisions.
        pageFields["size"] = Field<Page> { page: Page ->
            page.width = entry[1]!!.toInt().toFloat()
            page.height = entry[2]!!.toInt().toFloat()
        }
        pageFields["format"] = Field<Page> { page: Page ->
            page.format = Format.valueOf(
                entry[1]!!
            )
        }
        pageFields["filter"] = Field<Page> { page: Page ->
            page.minFilter = TextureFilter.valueOf(entry[1]!!)
            page.magFilter = TextureFilter.valueOf(entry[2]!!)
            page.useMipMaps = page.minFilter.isMipMap()
        }
        pageFields["repeat"] = Field<Page> { page: Page ->
            if (entry[1]!!.indexOf('x') != -1) page.uWrap = TextureWrap.Repeat
            if (entry[1]!!.indexOf('y') != -1) page.vWrap = TextureWrap.Repeat
        }
        pageFields["pma"] = Field<Page> { page: Page -> page.pma = entry[1] == "true" }
        val hasIndexes = booleanArrayOf(false)
        val regionFields: MutableMap<String?, Field<Region>> = HashMap(127, 0.99f) // Size needed to avoid collisions.
        // Deprecated, use bounds.
        regionFields["xy"] = Field<Region> { region: Region ->
            region.left = entry[1]!!.toInt()
            region.top = entry[2]!!.toInt()
        }
        // Deprecated, use bounds.
        regionFields["size"] = Field<Region> { region: Region ->
            region.width = entry[1]!!.toInt()
            region.height = entry[2]!!.toInt()
        }
        regionFields["bounds"] = Field<Region> { region: Region ->
            region.left = entry[1]!!.toInt()
            region.top = entry[2]!!.toInt()
            region.width = entry[3]!!.toInt()
            region.height = entry[4]!!.toInt()
        }
        // Deprecated, use offsets.
        regionFields["offset"] = Field { region: Region ->
            region.offsetX = entry[1]!!.toInt().toFloat()
            region.offsetY = entry[2]!!.toInt().toFloat()
        }
        // Deprecated, use offsets.
        regionFields["orig"] = Field { region: Region ->
            region.originalWidth = entry[1]!!.toInt()
            region.originalHeight = entry[2]!!.toInt()
        }
        regionFields["offsets"] = Field<Region> { region: Region ->
            region.offsetX = entry[1]!!.toInt().toFloat()
            region.offsetY = entry[2]!!.toInt().toFloat()
            region.originalWidth = entry[3]!!.toInt()
            region.originalHeight = entry[4]!!.toInt()
        }
        regionFields["rotate"] = Field<Region> { region: Region ->
            val value = entry[1]
            if (value == "true") region.degrees = 90 else if (value != "false") //
                region.degrees = value!!.toInt()
            region.rotate = region.degrees == 90
        }
        regionFields["index"] = Field<Region> { region: Region ->
            region.index = entry[1]!!.toInt()
            if (region.index != -1) hasIndexes[0] = true
        }
        val reader = BufferedReader(InputStreamReader(packFile.read()), 1024)
        try {
            var line = reader.readLine()
            // Ignore empty lines before first entry.
            while (line != null && line.trim { it <= ' ' }.isEmpty()) line = reader.readLine()
            // Header entries.
            while (true) {
                if (line == null || line.trim { it <= ' ' }.isEmpty()) break
                if (readEntry(entry, line) == 0) break // Silently ignore all header fields.
                line = reader.readLine()
            }
            // Page and region entries.
            var page: Page? = null
            var names: MutableList<String?>? = null
            var values: MutableList<IntArray?>? = null
            while (line != null) {
                if (line.trim { it <= ' ' }.isEmpty()) {
                    page = null
                    line = reader.readLine()
                } else if (page == null) {
                    page = Page(imagesDir.child(line))
                    while (readEntry(entry, reader.readLine().also { line = it }) != 0) {
                        val field = pageFields[entry[0]]
                        field?.parse(page) // Silently ignore unknown page fields.
                    }
                    pages.add(page)
                } else {
                    val region = Region(
                        page = page,
                        name = line!!.trim { it <= ' ' },
                    )
                    while (true) {
                        val count = readEntry(entry, reader.readLine().also { line = it })
                        if (count == 0) break
                        val field = regionFields[entry[0]]
                        if (field != null) field.parse(region) else {
                            if (names == null) {
                                names = ArrayList(8)
                                values = ArrayList(8)
                            }
                            names.add(entry[0])
                            val entryValues = IntArray(count)
                            for (i in 0 until count) {
                                try {
                                    entryValues[i] = entry[i + 1]!!.toInt()
                                } catch (ignored: NumberFormatException) { // Silently ignore non-integer values.
                                }
                            }
                            values!!.add(entryValues)
                        }
                    }
                    if (region.originalWidth == 0 && region.originalHeight == 0) {
                        region.originalWidth = region.width
                        region.originalHeight = region.height
                    }
                    if (!names.isNullOrEmpty() && !values.isNullOrEmpty()) {
                        region.names = names.filterNotNull()
                        region.values = values.filterNotNull()
                        names.clear()
                        values.clear()
                    }
                    regions.add(region)
                }
            }
        } catch (ex: Exception) {
            throw RuntimeException("Error reading texture atlas file: $packFile", ex)
        } finally {
            closeQuietly(reader)
        }
        if (hasIndexes[0]) {
            regions.sortWith { region1: Region, region2: Region ->
                var i1 = region1.index
                if (i1 == -1) i1 = Int.MAX_VALUE
                var i2 = region2.index
                if (i2 == -1) i2 = Int.MAX_VALUE
                i1 - i2
            }
        }
        this.regions = regions
        this.pages = pages
    }

    private fun interface Field<T> {
        fun parse(`object`: T)
    }

    class Page(val textureFile: File) {
        var width = 0f
        var height = 0f
        var useMipMaps = false
        var format = Format.RGBA8888
        var minFilter = TextureFilter.Nearest
        var magFilter = TextureFilter.Nearest
        var uWrap = TextureWrap.ClampToEdge
        var vWrap = TextureWrap.ClampToEdge
        var pma = false
    }

    class Region(
        val page: Page,
        val name: String,
    ) {
        var left = 0
        var top = 0
        var width = 0
        var height = 0
        var offsetX = 0f
        var offsetY = 0f
        var originalWidth = 0
        var originalHeight = 0
        var degrees = 0
        var rotate = false
        var index = -1
        var names: List<String>? = null
        var values: List<IntArray>? = null
        var flip = false
        fun findValue(name: String): IntArray? {
            if (names != null && values != null) {
                var i = 0
                val n = names!!.size
                while (i < n) {
                    if (name == names!![i]) return values!![i]
                    i++
                }
            }
            return null
        }
    }

    companion object {
        @Throws(IOException::class)
        private fun readEntry(entry: Array<String?>, line: String?): Int {
            var lineNotNull = line ?: return 0
            lineNotNull = lineNotNull.trim { it <= ' ' }
            if (lineNotNull.isEmpty()) return 0
            val colon = lineNotNull.indexOf(':')
            if (colon == -1) return 0
            entry[0] = lineNotNull.substring(0, colon).trim { it <= ' ' }
            var i = 1
            var lastMatch = colon + 1
            while (true) {
                val comma = lineNotNull.indexOf(',', lastMatch)
                if (comma == -1) {
                    entry[i] = lineNotNull.substring(lastMatch).trim { it <= ' ' }
                    return i
                }
                entry[i] = lineNotNull.substring(lastMatch, comma).trim { it <= ' ' }
                lastMatch = comma + 1
                if (i == 4) return 4
                i++
            }
        }
    }
}
