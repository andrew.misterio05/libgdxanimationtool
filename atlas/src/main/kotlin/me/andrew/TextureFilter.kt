package me.andrew

enum class TextureFilter {
    Nearest,
    Linear,
    MipMap,
    MipMapNearestNearest,
    MipMapLinearNearest,
    MipMapNearestLinear,
    MipMapLinearLinear;

    fun isMipMap(): Boolean = this !== Nearest && this !== Linear
}
