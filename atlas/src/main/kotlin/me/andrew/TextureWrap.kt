package me.andrew

enum class TextureWrap {
    MirroredRepeat, ClampToEdge, Repeat;
}
