package me.andrew

import java.io.Closeable
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

fun closeQuietly(c: Closeable?) {
    if (c != null) {
        try {
            c.close()
        } catch (ignored: Throwable) {
        }
    }
}

fun File.read(): InputStream = FileInputStream(this)

fun File.child(name: String): File = if (path.isEmpty()) File(name) else File(this, name)
