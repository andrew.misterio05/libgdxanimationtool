package me.andrew

import kotlinx.coroutines.CoroutineScope

interface EffHandler<E: Eff> {
    suspend fun CoroutineScope.execute(eff: E, dispatch: (Cmd) -> Unit)

    companion object {
        suspend fun <E: Eff> EffHandler<E>.execute(
            coroutineScope: CoroutineScope,
            eff: E,
            dispatch: (Cmd) -> Unit,
        ) = coroutineScope.execute(eff, dispatch)
    }
}
