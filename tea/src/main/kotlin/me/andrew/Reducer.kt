package me.andrew

typealias ReducerResult<S, E> = Pair<S, Set<E>>

fun interface Reducer<S, C, E> {
    operator fun invoke(state: S, cmd: C): ReducerResult<S, E>

    companion object {

        fun <S, E> result(state: S, vararg effs: E) = state to setOf(*effs)
        fun <S, E> result(state: S, effs: Set<E>) = state to effs

        fun <S> result(state: S) = state to emptySet<Nothing>()
    }
}
